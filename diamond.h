#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "imageTools.h"

/* @definition	:	Intervalle de couleurs de la carte sous forme entière
 * */
#define MIN_HEX_COLOR 0
#define MAX_HEX_COLOR 255

/* @definition	:	Coefficient de lissage de l'aléatoire
 * */
#define	SMOOTH_COEFF 0.1

/* @function
 *      Renvoie une valeur aléatoire comprise entre deux valeurs
 *      passées en paramètre
 * 
 * @param
 *      float valeurMin : Valeur minimum atteinte aléatoirement
 *      float valeurMax : Valeur maximum atteinte aléatoirement
 * 
 * @return
 *      float alea : Valeur aléatoire comprise ente valeurMin et valeurMax
 */
float randomFloatBetween(float minValue, float maxValue);


/* @function
 *      Renvoie une valeur aléatoire entière comprise entre deux valeurs
 *      passées en paramètre
 * 
 * @param
 *      int valeurMin : Valeur minimum atteinte aléatoirement
 *      int valeurMax : Valeur maximum atteinte aléatoirement
 * 
 * @return
 *      int : Valeur aléatoire comprise ente valeurMin et valeurMax
 */
int randomIntBetween(int minValue, int maxValue);


/* @function
 * 		Initialise les quatres coins d'une carte avec des valeurs aléatoires
 * 		comprises entre MIN_HEX_COLOR et MAX_HEX_COLOR
 * 
 * @param
 * 		field	: Pointeur vers la matrixImage à initialiser
 * 
 * @return
 * 		void
 * */
void initMapEdges(matrixImage *field);


/* @function
 * 		Effectue l'étape Diamant de l'algorithme du diamant carré
 * 		c'est à dire effectue la moyenne des quatres coins d'un carré
 * 		et donne cette valeur au centre du carré
 * 
 * @param
 * 		field		: Pointeur vers la matrixImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void diamondStep(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									 unsigned int coordHeight2, unsigned int coordLength2);


/* @function
 * 		Effectue l'étape Carré de l'algorithme du diamant carré
 * 		c'est à dire donne au centre de chaque côté du carré
 * 		la valeur de la moyenne des deux coins et du centre
 * 
 * @param
 * 		field		: Pointeur vers la matrixImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void squareStep(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									unsigned int coordHeight2, unsigned int coordLength2);


/* @function
 * 		Effectue l'algorithme du diamant carré sur une carte donnée.
 * 		Les coins de la carte doivent être initialisés au préalable
 *		(Fonction récursive)
 * 
 * @param
 * 		field		: Pointeur vers la matricImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void diamondSquare(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									   unsigned int coordHeight2, unsigned int coordLength2);


/* @function
 * 		Effectue l'algorithme du diamant carré sur une carte donnée.
 * 		La carte doit être initialisée avec la fonction initMapEdges
 * 		La carte doit être de dimentions 2^n + 1
 * 		Les valeurs finales ne correspondent pas à des valeurs de couleurs
 * 
 * @param
 * 		matrix		: Pointeur vers la matricImage à modifier
 * 
 * @return
 * 		void
 * */
void diamondSquareNonRecursive(matrixImage *matrix);

#include "diamond.h"

#define ERR_NB_ARG "Argument manquant : taille_carte => taille de la carte au format 2^n + 1"
#define ERR_FORMAT_ARG "Argument invalide : taille_carte => doit être sous la forme 2^n + 1   (ex : 1025)"



int main(int argc, char *argv[])
{
	//On vérifie que le nombre d'arguments est correct
	if(argc < 2)
	{
		printf("\033[0;31m%s\033[0m\n", ERR_NB_ARG);
		exit(1);
	}
	
	//Taille de la carte (argument)
	unsigned int map_size = atoi(argv[1]);
	
	//On vérifie l'argument est de la forme 2^n + 1
	if(map_size%2 == 0)
	{
		printf("\033[0;31m%s\033[0m\n", ERR_FORMAT_ARG);
		exit(1);
	}
	
	// Initialise l'aléatoire selon la date
	srand((unsigned int)time(NULL));
	
	// Tableau contenant les valeurs de chaque pixel de l'image
	printf("Génération de la carte de taille %d\n", map_size);
	matrixImage *matrix = createMatrix(map_size, map_size);
	
	// Donnees de la BMPLib pour enregistrer l'image
	DonneesImageRGB *image = NULL;

	if (matrix != NULL)
	{
		
		//Initialise les coins de la carte
		initMapEdges(matrix);
		printf("Génération de l'image... ");
		
		//Algorithme diamant-carré
		diamondSquareNonRecursive(matrix);
		
		//Met les valeurs de l'image entre 0 et 255 pour visualisation
		int heightIndex, lengthIndex;
		
		int max = (matrix->height -1)/2;
		int min = -(matrix->height -1)/2;
		int middle = (max + min)/2;
		
		for(heightIndex = 0; heightIndex < matrix->height; heightIndex++)
		{	
			for(lengthIndex = 0; lengthIndex < matrix->length; lengthIndex++)
			{
				short int value = matrix->green[heightIndex][lengthIndex];
				
				if(value > middle)
				{		
					matrix->green[heightIndex][lengthIndex] = rescale(value, middle, max, MIN_HEX_COLOR, MAX_HEX_COLOR);
				}
				else
				{
					matrix->green[heightIndex][lengthIndex] = 0;
					matrix->blue[heightIndex][lengthIndex] = rescale(value, min, middle, MIN_HEX_COLOR, MAX_HEX_COLOR);
				}
				
			}
		}	

		// Crée l'image
		image = matrixToBMP(*matrix);
		ecrisBMPRGB_Dans(image, "DiamondSquare.bmp");

		// Libère la mémoire allouée
		freeMatrix(&matrix);
		libereDonneesImageRGB(&image);
		
		printf("Terminé\n");
	}
	
	return 0;
}

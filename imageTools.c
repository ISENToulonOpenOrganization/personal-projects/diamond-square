#include "imageTools.h"

/* @fonction	:	Crée et alloue en mémoire une matrixImage et met
 * 					chaque pixel à 0
 * 					La structure ainsi crée devra être libérée en mémoire
 * 
 * @param		:	
 * 		length	:	Largeur de l'image destinée à cette matrice en pixels
 * 		height	:	Hauteur de l'image destinée à cette matrice en pixels
 * 
 * @retour		:	Pointeur vers la matrixImage crée
 * */
matrixImage *createMatrix(int length, int height)
{
	int i, j;
	
	matrixImage *matrix = (matrixImage*)malloc(sizeof(matrixImage));
	matrix->length = length;
	matrix->height = height;
	
	matrix->red = (short int**)malloc(height * sizeof(short int*));
	matrix->green = (short int**)malloc(height * sizeof(short int*));	
	matrix->blue = (short int**)malloc(height * sizeof(short int*));	
	
	for(i = 0; i < height; i++)
	{
		matrix->red[i] = (short int*)malloc(length * sizeof(short int));
		matrix->green[i] = (short int*)malloc(length * sizeof(short int));
		matrix->blue[i] = (short int*)malloc(length * sizeof(short int));
	}
	
	for(i = 0; i < height; i++)
	{
		for(j = 0; j < length; j++)
		{
			matrix->red[i][j] = 0;
			matrix->green[i][j] = 0;
			matrix->blue[i][j] = 0;
		}
	}
	
	return matrix;
}


/* @fonction	:	Libère la mémoire allouée à une matrixImage
 * 
 * @param		:
 * 		matrix	:	Adresse du pointeur vers la matrixImage à libérer
 * 
 * @retour		:	\
 * */
void freeMatrix(matrixImage **matrix)
{
	int i;
	
	for(i = 0; i < (*matrix)->height; i++)
	{
		free((*matrix)->red[i]);
		free((*matrix)->green[i]);
		free((*matrix)->blue[i]);		
	}

	free((*matrix)->red);
	free((*matrix)->green);
	free((*matrix)->blue);	
	free(*matrix);
}


/* @fonction	:	Crée et alloue en mémoire une DonneesImageRGB
 * 					La structure ainsi crée devra être libérée en mémoire
 * 
 * @param		:	
 * 		length	:	Largeur de l'image en pixels
 * 		height	:	Hauteur de l'image en pixels
 * 
 * @retour		:	Pointeur vers la DonneesImageRGB crée
 * */
DonneesImageRGB *createDonneesImageRGB(int length, int height)
{
	DonneesImageRGB *imageBMP = (DonneesImageRGB*)malloc(sizeof(DonneesImageRGB));
	
	imageBMP->largeurImage = length;
	imageBMP->hauteurImage = height;
	
	imageBMP->donneesRGB = (unsigned char *)malloc(3 * length * height * sizeof(unsigned char));
	
	return imageBMP;
}


/* @fonction	:	Renvoie la matrixImage correspondant à la structure
 * 					DonneesImageRGB passée en paramètre
 * 
 * @param		:
 * 		imageBMP:	DonneesImageRGB à convertir en matrixImage
 * 
 * @retour		:	Pointeur vers la matrixImage convertie
 * */
matrixImage *BMPtoMatrix(DonneesImageRGB imageBMP)
{
	matrixImage *matrix = createMatrix(imageBMP.largeurImage, imageBMP.hauteurImage);

	int imatrix = 0;
	int jmatrix = 0;
	int indexRGB;
	
	int sizeData = 3 * matrix->length * matrix->height;
	
	for(indexRGB = 0; indexRGB < sizeData; indexRGB += 3)
	{
		matrix->blue[imatrix][jmatrix] = imageBMP.donneesRGB[indexRGB];
		matrix->green[imatrix][jmatrix] = imageBMP.donneesRGB[indexRGB + 1];
		matrix->red[imatrix][jmatrix] = imageBMP.donneesRGB[indexRGB + 2];
		jmatrix += 1;
		
		if(jmatrix >= matrix->length)
		{
			jmatrix = 0;
			imatrix += 1;
		}
	}
	
	return matrix;
}


/* @fonction	:	Renvoie la DonneesImageRGB correspondant à la structure
 * 					matrixImage passée en paramètre
 * 
 * @param		:
 * 		matrix	:	matrixImage à convertir en DonneesImageRGB
 * 
 * @retour		:	Pointeur vers la DonneesImageRGB convertie
 * */
DonneesImageRGB *matrixToBMP(matrixImage matrix)
{
	DonneesImageRGB *imageBMP = createDonneesImageRGB(matrix.length, matrix.height);	
	
	int imatrix = 0;
	int jmatrix = 0;
	int indexRGB;
	
	int sizeData = 3 * imageBMP->largeurImage * imageBMP->hauteurImage;
	
	for(indexRGB = 0; indexRGB < sizeData; indexRGB += 3)
	{
		imageBMP->donneesRGB[indexRGB] = matrix.blue[imatrix][jmatrix];
		imageBMP->donneesRGB[indexRGB + 1] = matrix.green[imatrix][jmatrix];
		imageBMP->donneesRGB[indexRGB + 2] = matrix.red[imatrix][jmatrix];
		jmatrix += 1;
		
		if(jmatrix >= matrix.length)
		{
			jmatrix = 0;
			imatrix += 1;
		}
	}
	
	return imageBMP;
}

/* @fonction		:	Transforme un nombre flottant compris dans un
 * 						intervalle en un nombre compris dans un nouvel intervalle
 * 						en conservant les proportions
 * 
 * @param			:
 * 		value		:	Nombre à transformer
 * 		minValue	:	Borne inférieure de l'intervalle de ce nombre
 * 		maxValue	:	Borne supérieure de l'intervalle de ce nombre
 * 		newMin		:	Nouvelle borne inférieure
 * 		newMax		:	Nouvelle borne supérieure
 * 
 * @retour			:	Nombre compris entre nouveauMin et nouveauMax
 * */
float rescale(float value, float minValue, float maxValue, float newMin, float newMax)
{
	float valueRange = (maxValue - minValue);
	float newValue = (value + minValue) / valueRange;
	return newMin + newValue * (newMax - newMin);
}

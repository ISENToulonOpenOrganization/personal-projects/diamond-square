#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "BmpLib.h"

/* @structure	:	Contient les informations utiles au traitement d'une image
 * 
 * @param		:
 * 		length	:	Largeur de l'image en pixels	
 * 		height	:	Hauteur de l'image en pixels
 * 		red		:	Tableau 2D des valeurs de rouge de chaque pixel de l'image
 * 		green	:	Tableau 2D des valeurs de vert de chaque pixel de l'image
 * 		blue	:	Tableau 2D des valeurs de bleu de chaque pixel de l'image
 * */
typedef struct matrixImage
{
	int length;
	int height;
	short int **red;
	short int **green;
	short int **blue;
}matrixImage;


/* @fonction	:	Crée et alloue en mémoire une matrixImage et met
 * 					chaque pixel à 0
 * 					La structure ainsi crée devra être libérée en mémoire
 * 
 * @param		:	
 * 		length	:	Largeur de l'image destinée à cette matrice en pixels
 * 		height	:	Hauteur de l'image destinée à cette matrice en pixels
 * 
 * @retour		:	Pointeur vers la matrixImage crée
 * */
matrixImage *createMatrix(int length, int height);


/* @fonction	:	Libère la mémoire allouée à une matrixImage
 * 
 * @param		:
 * 		matrix	:	Adresse du pointeur vers la matrixImage à libérer
 * 
 * @retour		:	\
 * */
void freeMatrix(matrixImage **matrix);


/* @fonction	:	Crée et alloue en mémoire une DonneesImageRGB
 * 					La structure ainsi crée devra être libérée en mémoire
 * 
 * @param		:	
 * 		length	:	Largeur de l'image en pixels
 * 		height	:	Hauteur de l'image en pixels
 * 
 * @retour		:	Pointeur vers la DonneesImageRGB crée
 * */
DonneesImageRGB *createDonneesImageRGB(int length, int height);


/* @fonction	:	Renvoie la matrixImage correspondant à la structure
 * 					DonneesImageRGB passée en paramètre
 * 
 * @param		:
 * 		imageBMP:	DonneesImageRGB à convertir en matrixImage
 * 
 * @retour		:	Pointeur vers la matrixImage convertie
 * */
matrixImage *BMPtoMatrix(DonneesImageRGB imageBMP);


/* @fonction	:	Renvoie la DonneesImageRGB correspondant à la structure
 * 					matrixImage passée en paramètre
 * 
 * @param		:
 * 		matrix	:	matrixImage à convertir en DonneesImageRGB
 * 
 * @retour		:	Pointeur vers la DonneesImageRGB convertie
 * */
DonneesImageRGB *matrixToBMP(matrixImage matrix);


/* @fonction		:	Transforme un nombre flottant compris dans un
 * 						intervalle en un nombre compris dans un nouvel intervalle
 * 						en conservant les proportions
 * 
 * @param			:
 * 		value		:	Nombre à transformer
 * 		minValue	:	Borne inférieure de l'intervalle de ce nombre
 * 		maxValue	:	Borne supérieure de l'intervalle de ce nombre
 * 		newMin		:	Nouvelle borne inférieure
 * 		newMax		:	Nouvelle borne supérieure
 * 
 * @retour			:	Nombre compris entre nouveauMin et nouveauMax
 * */
float rescale(float value, float minValue, float maxValue, float newMin, float newMax);

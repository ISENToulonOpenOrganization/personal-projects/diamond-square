#include "diamond.h"


/* @function
 *      Renvoie une valeur aléatoire comprise entre deux valeurs
 *      passées en paramètre
 * 
 * @param
 *      float valeurMin : Valeur minimum atteinte aléatoirement
 *      float valeurMax : Valeur maximum atteinte aléatoirement
 * 
 * @return
 *      float alea : Valeur aléatoire comprise ente valeurMin et valeurMax
 */
float randomFloatBetween(float minValue, float maxValue)
{
    return ((float)rand()/(float)(RAND_MAX))*(maxValue - minValue) + minValue;
}


/* @function
 *      Renvoie une valeur aléatoire entière comprise entre deux valeurs
 *      passées en paramètre
 * 
 * @param
 *      int valeurMin : Valeur minimum atteinte aléatoirement
 *      int valeurMax : Valeur maximum atteinte aléatoirement
 * 
 * @return
 *      int : Valeur aléatoire comprise ente valeurMin et valeurMax
 */
int randomIntBetween(int minValue, int maxValue)
{
    return randomFloatBetween(minValue, maxValue);
}


/* @function
 * 		Initialise les quatres coins d'une carte avec des valeurs aléatoires
 * 		comprises entre MIN_HEX_COLOR et MAX_HEX_COLOR
 * 
 * @param
 * 		field	: Pointeur vers la matrixImage à initialiser
 * 
 * @return
 * 		void
 * */
void initMapEdges(matrixImage *field)
{
	int range = field->height / 2;
	// Coin inférieur gauche
	field->green[0][0] = randomIntBetween(-range, range);
	
	// Coin inférieur droit
	field->green[0][field->length - 1] = randomIntBetween(-range, range);
	
	// Coin supérieur gauche
	field->green[field->height - 1][0] = randomIntBetween(-range, range);
	
	// Coin supérieur droit
	field->green[field->height - 1][field->length - 1] = randomIntBetween(-range, range);
}


/* @function
 * 		Effectue l'étape Diamant de l'algorithme du diamant carré
 * 		c'est à dire effectue la moyenne des quatres coins d'un carré
 * 		et donne cette valeur au centre du carré
 * 
 * @param
 * 		field		: Pointeur vers la matrixImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void diamondStep(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									 unsigned int coordHeight2, unsigned int coordLength2)
{
	// Portée de l'aléatoire
	int range = (coordHeight2 - coordHeight1) / 2;
	
	// Index du centre du carré
	unsigned int centerLength = (coordLength1 + coordLength2)/2;
	unsigned int centerHeight = (coordHeight1 + coordHeight2)/2;
	
	// Moyenne des 4 points
	field->green[centerHeight][centerLength] = (field->green[coordHeight1][coordLength1] 
											  + field->green[coordHeight2][coordLength1] 
											  + field->green[coordHeight1][coordLength2] 
											  + field->green[coordHeight2][coordLength2]) / 4;
						
	// Composante aléatoire
	field->green[centerHeight][centerLength] += randomIntBetween(-range, range) * SMOOTH_COEFF;
}


/* @function
 * 		Effectue l'étape Carré de l'algorithme du diamant carré
 * 		c'est à dire donne au centre de chaque côté du carré
 * 		la valeur de la moyenne des deux coins et du centre
 * 
* @param
 * 		field		: Pointeur vers la matrixImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void squareStep(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									unsigned int coordHeight2, unsigned int coordLength2)
{
	// Portée de l'aléatoire
	int range = (coordHeight2 - coordHeight1) / 2;
	
	// Index du centre du carré
	unsigned int centerLength = (coordLength1 + coordLength2)/2;
	unsigned int centerHeight = (coordHeight1 + coordHeight2)/2;
	
	// Calculs des moyennes
	//Gauche
	field->green[centerHeight][coordLength1] = (field->green[centerHeight][centerLength] 
			+ field->green[coordHeight1][coordLength1] + field->green[coordHeight2][coordLength1])/3;
	//Droite
	field->green[centerHeight][coordLength2] = (field->green[centerHeight][centerLength]
			+ field->green[coordHeight1][coordLength2] + field->green[coordHeight2][coordLength2])/3;
	//Haut
	field->green[coordHeight2][centerLength] = (field->green[centerHeight][centerLength]
			+ field->green[coordHeight2][coordLength1] + field->green[coordHeight2][coordLength2])/3;
	//Bas
	field->green[coordHeight1][centerLength] = (field->green[centerHeight][centerLength]
			+ field->green[coordHeight1][coordLength1] + field->green[coordHeight1][coordLength2])/3;
	
	// Composante aléatoire
	
	field->green[centerHeight][coordLength1] += randomIntBetween(-range, range) * SMOOTH_COEFF;
	field->green[centerHeight][coordLength2] += randomIntBetween(-range, range) * SMOOTH_COEFF;
	field->green[coordHeight2][centerLength] += randomIntBetween(-range, range) * SMOOTH_COEFF;
	field->green[coordHeight1][centerLength] += randomIntBetween(-range, range) * SMOOTH_COEFF;
}


/* @function
 * 		Effectue l'algorithme du diamant carré sur une carte donnée.
 * 		Les coins de la carte doivent être initialisés au préalable
 *		(Fonction récursive)
 * 
 * @param
 * 		field		: Pointeur vers la matricImage à modifier
 * 		coordHeight1: Coordonnée en hauteur du coin inférieur gauche du carré
 * 		coordLength1: Coordonnée en largeur du coin inférieur gauche du carré
 * 		coordHeight2: Coordonnée en hauteur du coin supérieur droit du carré
 * 		coordLength2: Coordonnée en largeur du coin supérieur droit du carré
 * 
 * @return
 * 		void
 * */
void diamondSquare(matrixImage *field, unsigned int coordHeight1, unsigned int coordLength1,
									   unsigned int coordHeight2, unsigned int coordLength2)
{
	if((coordLength2 - coordLength1) > 1 && (coordHeight2 - coordHeight1) > 1)
	{	
		// Effectue les deux étapes de l'algorithme sur le carré actuel
		diamondStep(field, coordHeight1, coordLength1, coordHeight2, coordLength2);
		squareStep(field, coordHeight1, coordLength1, coordHeight2, coordLength2);

		// Index du centre du carré
		unsigned int centerLength = (coordLength1 + coordLength2)/2;
		unsigned int centerHeight = (coordHeight1 + coordHeight2)/2;
			
		// Traite les 4 carrés contenus dans le carré actuel
		//Bas gauche
		diamondSquare(field, coordHeight1, coordLength1, centerHeight, centerLength);
		//Bas droite
		diamondSquare(field, coordHeight1, centerLength, centerHeight, coordLength2);
		//Haut gauche
		diamondSquare(field, centerHeight, coordLength1, coordHeight2, centerLength);
		//Haut droite
		diamondSquare(field, centerHeight, centerLength, coordHeight2, coordLength2); 
	}		
}


/* @function
 * 		Effectue l'algorithme du diamant carré sur une carte donnée.
 * 
 * @param
 * 		matrix		: Pointeur vers la matricImage à modifier
 * 
 * @return
 * 		void
 * */
void diamondSquareNonRecursive(matrixImage *matrix)
{
    initMapEdges(matrix);
    
    int i = matrix->height - 1;
	int x, y, average, id, lag, sum, n;
    
    while(i > 1)
    {
        id = i/2;
        
        // Diamond step
        for(x = id; x < matrix->height; x += i)
        {
			for(y = id; y < matrix->height; y += i)
			{ 
                average = (matrix->green[x - id][y - id] + matrix->green[x - id][y + id] 
						 + matrix->green[x + id][y + id] + matrix->green[x + id][y - id]) / 4;
                matrix->green[x][y] = average + randomIntBetween(-id, id);
            }
        }
        
        lag = 0;

		// Square step
        for(x = 0; x < matrix->height; x += id)
		{
			if(lag == 0)
			{
				lag = id;
			}
			else
			{
				lag = 0;
			}
			
			for(y = lag; y < matrix->height; y += i)
			{
                sum = 0;
                n = 0;
                
                if(x >= id)
				{
					sum += matrix->green[x - id][y];
					n += 1;
				}
                if(x + id < matrix->height)
                {
                    sum += matrix->green[x + id][y];
                    n += 1;
                }
                if(y >= id)
                {
                    sum += matrix->green[x][y - id];
                    n += 1;
                }
                if(y + id < matrix->height)
                {
                    sum += matrix->green[x][y + id];
                    n += 1;
                }
                
                matrix->green[x][y] = (sum/n) + randomIntBetween(-id, id);
			}
        }
        
        i = id;
	}
}


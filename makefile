CFLAGS = #-Wall
CC = gcc
DEP = main.o BmpLib.o OutilsLib.o imageTools.o diamond.o -lm
EXEC = main

all: $(EXEC)

$(EXEC): $(DEP)
	@echo "Création de l'exécutable $@"
	$(CC) -o $@ $^
	@echo ""

%.o: %.c
	@echo "Compilation de $@"
	$(CC) -c $^ $(CFLAGS)
	@echo ""

clean: 
	@echo "Suppression des fichiers temporaires"
	rm -rf *.bmp *.o $(EXEC)
	@echo ""
